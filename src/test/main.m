/*
 * main.m
 * Main code file for the libobjctermio demo program
 * Created on 8/26/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License, version 3
 */

//imports
#import <Foundation/Foundation.h>
#import "../lib/objctermio.h"

//main function - main entry point for the program
int main(int argc, const char* argv[]) {
	//get an autorelease pool
	NSAutoreleasePool* pool = [[NSAutoreleasePool alloc] init];
	
	//write a prompt to the terminal
	[Terminal writeString: @"How many stars would you like?\n"];

	//read a number from the terminal
	NSNumber* starCount = [Terminal readNumber];

	//print out that many stars
	int i;
	for(i = 0; i < [starCount intValue]; i++) {
		[Terminal writeChar: '*'];
	}

	//write a newline
	[Terminal writeChar: '\n'];

	//drain the pool
	[pool drain];

	//and exit with no errors
	return EXIT_SUCCESS;
}

//end of program
