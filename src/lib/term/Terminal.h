/*
 * Terminal.h
 * Declares a class that represents a terminal interface
 * Created on 8/26/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License, version 3
 */

//import
#import <Foundation/Foundation.h>

//class declaration
@interface Terminal : NSObject {
	//no fields
}

//no properties

//method declarations

//writes a string to the terminal
+ (void) writeString: (NSString*) str;

//writes a number to the terminal
+ (void) writeNumber: (NSNumber*) num;

//writes a character to the terminal
+ (void) writeChar: (char) ch;

//reads a string from the terminal
+ (NSString*) readString;

//reads a number from the terminal
//Will return nil if the read data is not a number
+ (NSNumber*) readNumber;

//reads a character from the terminal
+ (char) readChar;

@end //end of header
