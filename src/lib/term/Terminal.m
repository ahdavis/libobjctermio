/*
 * Terminal.m
 * Implements a class that represents a terminal interface
 * Created on 8/26/2018
 * Created by Andrew Davis
 *
 * Copyright (C) 2018  Andrew Davis
 *
 * Licensed under the Lesser GNU General Public License, version 3
 */

//import the class header
#import "Terminal.h"

//import C standard I/O
#import <stdio.h>

@implementation Terminal

//writeString class method - writes a string to the terminal
+ (void) writeString: (NSString*) str {
	//get an NSData object from the string
	NSData* data = [str dataUsingEncoding: NSUTF8StringEncoding];

	//get a stdout file handle
	NSFileHandle* handle = [NSFileHandle fileHandleWithStandardOutput];

	//and write the data to stdout
	[handle writeData: data];
}

//writeNumber class method - writes a number to the terminal
+ (void) writeNumber: (NSNumber*) num {
	//call the writeString method
	[Terminal writeString: [num stringValue]];
}

//writeChar class method - writes a character to the terminal
+ (void) writeChar: (char) ch {
	//call the writeString method
	[Terminal writeString: 
   		[NSString stringWithFormat: @"%c", ch]];
}

//readString class method - reads a string from the terminal
+ (NSString*) readString {
	//get a stdin file handle
	NSFileHandle* handle = [NSFileHandle fileHandleWithStandardInput];

	//get the available data from the handle
	NSData* data = [handle availableData];

	//turn the data into a string
	NSString* input = [[NSString alloc] 
				initWithData: data 
				    encoding: NSUTF8StringEncoding];

	//trim the newline from the string
	NSCharacterSet* set = [NSCharacterSet newlineCharacterSet];
	NSString* userInput = [NSString stringWithFormat: @"%@",
			[input stringByTrimmingCharactersInSet: set]];

	//release the input string
	[input release];

	//and return the trimmed string
	return userInput;
}

//readNumber class method - reads a number from the terminal
+ (NSNumber*) readNumber {
	//read a string from stdout
	NSString* numStr = [Terminal readString];

	//get a number formatter to convert the string to a number with
	NSNumberFormatter* f = [[NSNumberFormatter alloc] init];

	//set the formatter's number style
	[f setNumberStyle: NSNumberFormatterDecimalStyle];

	//convert the string to a number
	NSNumber* ret = [f numberFromString: numStr];

	//release the number formatter
	[f release];

	//and return the converted number
	return ret;
}

//readChar class method - reads a character from the terminal
+ (char) readChar {
	return getchar(); //read and return a character
}

@end //end of implementation
