# Makefile for libobjctermio
# Compiles the code for the library
# Created on 8/26/2018
# Created by Andrew Davis
#
# Copyright (C) 2018  Andrew Davis
#
# Licensed under the Lesser GNU General Public License, version 3

# define the compiler
CC=gcc

# define the compiler flags for the library
CFLAGS=`gnustep-config --objc-flags` -x objective-c -fPIC -c -Wall
CFLAGS += -Wno-unused-but-set-variable

# define the compiler flags for the test program
test: CFLAGS=`gnustep-config --objc-flags` -x objective-c -c -Wall -g

# define linker flags for the library
LDFLAGS=-shared -lgnustep-base -lobjc 

# define linker flags for the test program
TFLAGS=-lgnustep-base -lobjc -L./lib -Wl,-rpath,./lib -lobjctermio

# retrieve source code for the library
LTERM=$(shell ls src/lib/term/*.m)

# list the source code for the library
LSOURCES=$(LTERM)

# compile the source code for the library
LOBJECTS=$(LSOURCES:.m=.o)

# retrieve source code for the test program
TMAIN=$(shell ls src/test/*.m)

# list the source code for the test program
TSOURCES=$(TMAIN)

# compile the source code for the test program
TOBJECTS=$(TSOURCES:.m=.o)

# define the name of the library
LIB=libobjctermio.so

# define the name of the test executable
TEST=demo

# rule for building both the library and the test program
all: library test

# master rule for compiling the library
library: $(LSOURCES) $(LIB)

# master rule for compiling the test program
test: $(TSOURCES) $(TEST)

# sub-rule for compiling the library
$(LIB): $(LOBJECTS)
	$(CC) $(LOBJECTS) -o $@ $(LDFLAGS)
	mkdir lib
	mkdir lobj
	mv -f $(LOBJECTS) lobj/
	mv -f $@ lib/
	find . -type f -name '*.d' -delete

# sub-rule for compiling the test program
$(TEST): $(TOBJECTS)
	$(CC) $(TOBJECTS) -o $@ $(TFLAGS)
	mkdir bin
	mkdir tobj
	mv -f $(TOBJECTS) tobj/
	mv -f $@ bin/
	find . -type f -name '*.d' -delete

# rule for compiling source code to object code
.m.o:
	$(CC) $(CFLAGS) $< -o $@

# target to install the compiled library
# REQUIRES ROOT
install:
	if [ -f "/usr/lib/libobjctermio.so" ]; then \
		rm /usr/lib/libobjctermio.so; \
	fi 
	if [ -d "/usr/include/objctermio" ]; then \
		rm -rf /usr/include/objctermio; \
	fi 
	
	mkdir /usr/include/objctermio
	mkdir /usr/include/objctermio/term  
	cp src/lib/objctermio.h /usr/include/objctermio/
	cp $(shell ls src/lib/term/*.h) /usr/include/objctermio/term/
	cp ./lib/$(LIB) /usr/lib/

# target to clean the workspace
clean:
	if [ -d "lobj" ]; then \
		rm -rf lobj; \
	fi
	if [ -d "lib" ]; then \
		rm -rf lib; \
	fi
	if [ -d "bin" ]; then \
		rm -rf bin; \
	fi
	if [ -d "tobj" ]; then \
		rm -rf tobj; \
	fi

# end of Makefile
